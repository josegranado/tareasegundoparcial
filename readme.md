## INTEGRANTES
    +José Granado
    +Angel Sucasas
    
## GET

    /api/v1/clients:
        Con esta ENDPOINT a través del metodo GET obtienes todos los clientes del sistema.
    /api/v1/clients/{id_client}/transactions
        Con esta ENDPOINT a través del metodo GET obtienes todas las transacciones de un cliente a partir de su id.


## POST
    
    /api/v1/clients:
        Con esta ENDPOINT a través del metodo POST puedes crear un cliente.
        Ejemplo del JSON:
        {
            "first_name":"Jose",
            "last_name":"Granado",
            "email":"jose.granado2014@gmail.com",
            "password":"contraseña"
        }
    /api/v1/clients/{id_client}/transactions
        Con esta ENDPOINT a través del metodo POST puedes crear una transacción a partir del id del cliente.
        EJEMPLO del JSON:
        {
            "amount":"1006.90",
            "purchase_date":"2018-12-28"
        }

## PUT
    /api/v1/clients/{id_cliente}:
        Con esta ENDPOINT a través del metodo PUT puedes editar un cliente.
        Ejemplo del JSON:
        {
            "first_name":"Jose",
            "last_name":"Granado",
            "email":"jose.granado2014@gmail.com",
            "password":"contraseña"
        }
    /api/v1/clients/{id_client}/transactions/{id_transaction}
        Con esta ENDPOINT a través del metodo PUT puedes editar una transacción a partir del id del cliente.
        EJEMPLO del JSON:
        {
            "amount":"1006.90",
            "purchase_date":"2018-12-28"
        }

## DELETE
    /api/v1/clients/{id_cliente}
        Con esta ENDPOINT a través del metodo DELETE puedes eliminar un cliente.
    /api/v1/clients/{id_cliente}/transactions/{id_transaction}
        Con esta ENDPOINT a través del metodo DELETE puedes eliminar una transacción a partir del id del cliente.


## DEMÁS RUTAS
    GET /api/v1/clients/{id_cliente}
        Muestra un cliente a partir de su ID
    GET /api/v1/clients/{id_cliente}/transaction/{id_transaction}
        Muestra una transaccion a partir de su ID y del ID del cliente.


