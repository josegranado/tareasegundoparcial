<?php

use Illuminate\Database\Seeder;
use App\Transaction;
use App\Client;
use Faker\Factory as Faker;

class TransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $cuantos = Client::all()->count();
        for ($i=0; $i<20; $i++)
		{
			// Cuando llamamos al método create del Modelo Fabricante 
			// se está creando una nueva fila en la tabla.
			Transaction::create(
				[
					'client_id' => $faker->numberBetween(1,$cuantos),
					'amount'=> $faker->randomFloat(2, 0, 1000),
					'purchase_date'=>$faker->date('Y-m-d','now')
				]
			);
		}
    }
}
