<?php

use Illuminate\Database\Seeder;
use App\Client;
use Faker\Factory as Faker;
class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i=0; $i<20; $i++)
		{
			// Cuando llamamos al método create del Modelo Fabricante 
			// se está creando una nueva fila en la tabla.
			Client::create(
				[
					'first_name'=>$faker->firstName(),
					'last_name'=>$faker->lastName(),
					'email'=>$faker->email(),
					'password'=> str_random(15)
				]
			);
		}
    }
}
