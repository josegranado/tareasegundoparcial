<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use Illuminate\Support\Facades\Response;

class ClientController extends Controller
{
   
    public function index()
    {
        $clients = Client::all();
        if ($clients)
        {
            $response = Response::json($clients,201);
            return $response;
        }
        else
        {
            $error = Response::json(['Error' => 'Unauthorized'], 401);  
            return $error;
        }
    }

    public function create()
    {
        
    }

    public function store(Request $request)
    {
        $data = $request->json()->all();
        if ($data)
        {
            $client = Client::create([
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
                'password' => $data['password'],
            ]);
            $response = Response::json($client, 201);
            return $response;
        }  
        else
        {
            $error = Response::json(['Error' => 'Unauthorized'], 401);  
            return $error; 
        }
    }

    public function show($id)
    {
        $client = Client::find($id);

        if ($client)
        {
            $response = Response::json($client,201);
            return $response;
        }
        else
        {
            $error = Response::json(['Error' => 'No user'],401);
            return $error;
        }
    }

    public function edit($id)
    {
        //
    }
    public function update(Request $request, $id)
    {
        $client = Client::find($id);
        $data = $request->json()->all();
        $flag = false;
        if (!$client)
        {
            $error = Response::json(['Error' => 'No user'], 401);
            return $error;
        }
        if ($data)
        {
            if($data['first_name'])
            {
              $first_name = $data['first_name']; 
              $client->first_name = $first_name;
              $flag = true; 
            }
            else
            {
                $first_name = null;
            }
            if($data['last_name'])
            {
              $last_name = $data['last_name']; 
              $client->last_name = $last_name;
              $flag = true; 
            }
            else
            {
                $last_name = null;
            }
            if ($data['email'])
            {
               $email = $data['email']; 
               $client->email = $email;
               $flag = true;
            }
            else
            {
                $email = null;
            }
            if ($data['password'])
            {
                $password = $data['password'];
                $client->password = $password;
                $flag = true;
            } else
            {
                $password = null;
            }
            if ($flag)
            {
                $client->save();   
            }   
            $response = Response::json($client, 201);
            return $response;
        } else
        {
            $noData = Response::json(['Error' => 'No data'], 401);
            return $noData;
        }
    }
    public function destroy($id)
    {
        $client = Client::find($id);
        if ($client)
        {
            $client->delete();
            $response = Response::json(['Message' => 'Usuario eliminado con éxito'],201);
            return $response;
        }
        else
        {
            $error = Response::json(['Error' => 'No client found.'],401);
            return $error;
        }
    }
}
