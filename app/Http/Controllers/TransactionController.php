<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Transaction;
use Illuminate\Support\Facades\Response;
class TransactionController extends Controller
{
    
    public function index($id)
    {
        $transactions = Transaction::where('client_id', $id)->get();
        if ($transactions)
        {
            $response = Response::json($transactions,201);
            return $response;
        }
        else
        {
            $error = Response::json(['Error' => 'No transactions found'],401);
            return $error;
        }
    }

    public function create()
    {
        /*$data = $request->json()->all();
        if ($data)
        {
            $transaction = Transaction::create([
                'client_id' => $id,
                'amount' => $data['amount'],
                'purchase_date' => $data['purchase_date']
        ]);
        $response = Response::json($transaction, 201);
        return $response;*/
    }

    public function store(Request $request, $id)
    {
        $data = $request->json()->all();
        if ($data)
        {
            $transaction = Transaction::create([
                'client_id' => $id,
                'amount' => $data['amount'],
                'purchase_date' => $data['purchase_date']
            ]);
            $response = Response::json($transaction, 201);
            return $response;
        }
        else
        {
            $error = Response::json(['Error' => 'No data'],401);
            return $error;
        }
    }
    public function show($idClient, $idTransaction)
    {
        $client = Client::find($idClient);
        $transaction = Transaction::find($idTransaction);
        if( $client )
        {
            if ($transaction)
            {
                $response = Response::json($transaction,201);
                return $response;
            }
            else
            {
                $error = Response::json(['Error' => 'No transaction found.'],401);
                return $error;
            }  
        }
        else
        {
            $noClient = Response::json(['Error' => 'No client found.'],401);
            return $noClient;
        }
        

    }
    public function edit($id)
    {
        //
    }
    public function update(Request $request, $idClient, $idTransaction)
    {
        $data = $request->json()->all();
        $client = Client::find($idClient);
        $transaction = Transaction::find($idTransaction);
        if ($data)
        {
            if ($client)
            {
                if( $transaction )
                {
                    $amount = $data['amount'];
                    $purchase_date = $data['purchase_date'];
                    $transaction->amount = $amount;
                    $transaction->purchase_date = $purchase_date;
                    $transaction->save();
                    $response = Response::json($transaction,201);
                    return $response;
                }
                else
                {
                    $ee = Response::json(['Error' => 'No transaction found.'], 401);
                    return $ee;
                }
            }
            else
            {
                $error = Response::json(['Error' => 'No client found.'],401);
                return $error;
            }
        }
        else
        {
            $e = Response::json(['Error' => 'No data'],401);
            return $e;
        }
    }
    public function destroy($idClient, $idTransaction)
    {
        $client = Client::find($idClient);
        if ($client)
        {
            $transaction = Transaction::find($idTransaction);
            if ($transaction)
            {
                $transaction->delete();
                $response = Response::json(['Message' => 'Transacion eliminada con éxito'],201);
                return $response;
            }
            else
            {
                $ee = Response::json(['Error' => 'No transaction found'],401);
                return $ee;
            }
        }
        else
        {
            $e = Response::json(['Error' => 'No client found'],401);
            return $e;
        }
    }
}
