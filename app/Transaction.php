<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transactions';
    protected $fillable = [
    	'client_id', 'amount', 'purchase_date',
    ];
    protected $hidden = [
    	'created_at', 'updated_at',
    ];

    public function client()
    {
    	return $this->belongsTo('App\Client');
    }
}
